import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'el-angular-packages';
  test = [
    { id: '1', text: 'test1' },
    { id: '2', text: 'test12' },
  ];
}
