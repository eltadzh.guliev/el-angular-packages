import * as i0 from '@angular/core';
import { EventEmitter, Component, Input, Output, NgModule } from '@angular/core';
import * as i1 from '@angular/common';
import { CommonModule } from '@angular/common';

class TabsSmallComponent {
    constructor() {
        this.selectedIdChange = new EventEmitter();
    }
    ngOnInit() { }
    onTabClick(id) {
        this.selectedId = id;
        this.selectedIdChange.emit(id);
    }
}
TabsSmallComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
TabsSmallComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: TabsSmallComponent, selector: "app-tabs-small", inputs: { tabs: "tabs", selectedId: "selectedId" }, outputs: { selectedIdChange: "selectedIdChange" }, ngImport: i0, template: "<button\r\n  *ngFor=\"let tab of tabs\"\r\n  class=\"tabs-small__button\"\r\n  [class.active]=\"tab.id === selectedId\"\r\n  (click)=\"onTabClick(tab.id)\"\r\n>\r\n  {{ tab.text }}\r\n</button>\r\n", styles: [":host{display:inline-flex;align-items:center;overflow:auto;padding:2px;border-radius:12px;background-color:#f5f5f5}:host .tabs-small__button{display:block;padding:12px 24px;font-size:14px;font-weight:500;line-height:20px;border-radius:12px;color:#222;cursor:pointer;background-color:unset;border:none;margin-right:4px}:host .tabs-small__button:last-child{margin-right:0}:host .tabs-small__button:hover{background-color:#fbfbfb}:host .tabs-small__button.active{background-color:#fff}\n"], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallComponent, decorators: [{
            type: Component,
            args: [{ selector: 'app-tabs-small', template: "<button\r\n  *ngFor=\"let tab of tabs\"\r\n  class=\"tabs-small__button\"\r\n  [class.active]=\"tab.id === selectedId\"\r\n  (click)=\"onTabClick(tab.id)\"\r\n>\r\n  {{ tab.text }}\r\n</button>\r\n", styles: [":host{display:inline-flex;align-items:center;overflow:auto;padding:2px;border-radius:12px;background-color:#f5f5f5}:host .tabs-small__button{display:block;padding:12px 24px;font-size:14px;font-weight:500;line-height:20px;border-radius:12px;color:#222;cursor:pointer;background-color:unset;border:none;margin-right:4px}:host .tabs-small__button:last-child{margin-right:0}:host .tabs-small__button:hover{background-color:#fbfbfb}:host .tabs-small__button.active{background-color:#fff}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { tabs: [{
                type: Input
            }], selectedId: [{
                type: Input
            }], selectedIdChange: [{
                type: Output
            }] } });

class TabsSmallModule {
}
TabsSmallModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
TabsSmallModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, declarations: [TabsSmallComponent], imports: [CommonModule], exports: [TabsSmallComponent] });
TabsSmallModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, imports: [CommonModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: TabsSmallModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        TabsSmallComponent
                    ],
                    imports: [
                        CommonModule
                    ],
                    exports: [
                        TabsSmallComponent
                    ]
                }]
        }] });

/*
 * Public API Surface of components
 */

/**
 * Generated bundle index. Do not edit.
 */

export { TabsSmallComponent, TabsSmallModule };
//# sourceMappingURL=components.mjs.map
