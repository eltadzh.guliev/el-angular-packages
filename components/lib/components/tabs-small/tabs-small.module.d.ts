import * as i0 from "@angular/core";
import * as i1 from "./components/tabs-small/tabs-small.component";
import * as i2 from "@angular/common";
export declare class TabsSmallModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<TabsSmallModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<TabsSmallModule, [typeof i1.TabsSmallComponent], [typeof i2.CommonModule], [typeof i1.TabsSmallComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<TabsSmallModule>;
}
