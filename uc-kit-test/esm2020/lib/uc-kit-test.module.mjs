import { NgModule } from '@angular/core';
import { UcKitTestComponent } from './uc-kit-test.component';
import * as i0 from "@angular/core";
export class UcKitTestModule {
}
UcKitTestModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
UcKitTestModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestModule, declarations: [UcKitTestComponent], exports: [UcKitTestComponent] });
UcKitTestModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestModule });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        UcKitTestComponent
                    ],
                    imports: [],
                    exports: [
                        UcKitTestComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWMta2l0LXRlc3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vcHJvamVjdHMvdWMta2l0LXRlc3Qvc3JjL2xpYi91Yy1raXQtdGVzdC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQzs7QUFjN0QsTUFBTSxPQUFPLGVBQWU7OzRHQUFmLGVBQWU7NkdBQWYsZUFBZSxpQkFSeEIsa0JBQWtCLGFBS2xCLGtCQUFrQjs2R0FHVCxlQUFlOzJGQUFmLGVBQWU7a0JBVjNCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFO3dCQUNaLGtCQUFrQjtxQkFDbkI7b0JBQ0QsT0FBTyxFQUFFLEVBQ1I7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGtCQUFrQjtxQkFDbkI7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVWNLaXRUZXN0Q29tcG9uZW50IH0gZnJvbSAnLi91Yy1raXQtdGVzdC5jb21wb25lbnQnO1xuXG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgVWNLaXRUZXN0Q29tcG9uZW50XG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIFVjS2l0VGVzdENvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFVjS2l0VGVzdE1vZHVsZSB7IH1cbiJdfQ==