import * as i0 from '@angular/core';
import { Injectable, Component, NgModule } from '@angular/core';

class UcKitTestService {
    constructor() { }
}
UcKitTestService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
UcKitTestService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class UcKitTestComponent {
}
UcKitTestComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
UcKitTestComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.9", type: UcKitTestComponent, selector: "lib-uc-kit-test", ngImport: i0, template: `
    <p>
      uc-kit-test works!
    </p>
  `, isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestComponent, decorators: [{
            type: Component,
            args: [{ selector: 'lib-uc-kit-test', template: `
    <p>
      uc-kit-test works!
    </p>
  ` }]
        }] });

class UcKitTestModule {
}
UcKitTestModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
UcKitTestModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestModule, declarations: [UcKitTestComponent], exports: [UcKitTestComponent] });
UcKitTestModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestModule });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.9", ngImport: i0, type: UcKitTestModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        UcKitTestComponent
                    ],
                    imports: [],
                    exports: [
                        UcKitTestComponent
                    ]
                }]
        }] });

/*
 * Public API Surface of uc-kit-test
 */

/**
 * Generated bundle index. Do not edit.
 */

export { UcKitTestComponent, UcKitTestModule, UcKitTestService };
//# sourceMappingURL=uc-kit-test.mjs.map
