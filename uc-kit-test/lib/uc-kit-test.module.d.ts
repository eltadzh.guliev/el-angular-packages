import * as i0 from "@angular/core";
import * as i1 from "./uc-kit-test.component";
export declare class UcKitTestModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<UcKitTestModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<UcKitTestModule, [typeof i1.UcKitTestComponent], never, [typeof i1.UcKitTestComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<UcKitTestModule>;
}
