import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsSmallComponent } from './components/tabs-small/tabs-small.component';

@NgModule({
  declarations: [
    TabsSmallComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TabsSmallComponent
  ]
})
export class TabsSmallModule { }
