import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TabSmall } from '../../interfaces/tab-small.interface';

@Component({
  selector: 'app-tabs-small',
  templateUrl: './tabs-small.component.html',
  styleUrls: ['./tabs-small.component.sass'],
})
export class TabsSmallComponent implements OnInit {
  @Input() tabs!: TabSmall[];
  @Input() selectedId!: string;

  @Output() selectedIdChange = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  onTabClick(id: string): void {
    this.selectedId = id;
    this.selectedIdChange.emit(id);
  }
}
