import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UcKitTestComponent } from './uc-kit-test.component';

describe('UcKitTestComponent', () => {
  let component: UcKitTestComponent;
  let fixture: ComponentFixture<UcKitTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UcKitTestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UcKitTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
