import { NgModule } from '@angular/core';
import { UcKitTestComponent } from './uc-kit-test.component';



@NgModule({
  declarations: [
    UcKitTestComponent
  ],
  imports: [
  ],
  exports: [
    UcKitTestComponent
  ]
})
export class UcKitTestModule { }
