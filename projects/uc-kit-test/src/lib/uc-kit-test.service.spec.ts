import { TestBed } from '@angular/core/testing';

import { UcKitTestService } from './uc-kit-test.service';

describe('UcKitTestService', () => {
  let service: UcKitTestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UcKitTestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
