/*
 * Public API Surface of uc-kit-test
 */

export * from './lib/uc-kit-test.service';
export * from './lib/uc-kit-test.component';
export * from './lib/uc-kit-test.module';
